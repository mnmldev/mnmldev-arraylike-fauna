const arraylike = require('./../index')

let arr = [{
  key: "_alkey_1",
  data: { name: "Gabin" }
}, {
  key: "_alkey_2",
  data: { name: "Charlotte" }
}, {
  key: "_alkey_3",
  data: { name: "Dove" }
}]

let toObj = arraylike.toObject(arr)
console.log("toObj", toObj);

let obj = {
  key_1: {
    data: { name: "Gabin" },
    key: "_alkey_1",
    index: 0
  },
  key_2: {
    data: { name: "Charlotte" },
    key: "_alkey_2",
    index: 1
  },
  key_3: {
    data: { name: "Dove" },
    key: "_alkey_3",
    index: 2
  },
  __arrayLike: true
}

let toArr = arraylike.toArray(obj)
// console.log("toArr", toArr);

let fromDeepObj = {
  arr1: [{
    key: "_alkey_1",
    data: { name: "Gabin" }
  }, {
    key: "_alkey_2",
    name: "Charlotte"
  }, {
    key: "_alkey_3",
    _adata: { name: "Dove" }
  }],
  sub: {
    arr2: [{
      key: "_alkey_1",
      data: { name: "Gabin" }
    }, {
      key: "_alkey_2",
      data: { name: "Charlotte" }
    }, {
      key: "_alkey_3",
      data: { name: "Dove" }
    }]
  },
  empty: []
}

let toDeepObj = arraylike.toObjectDeep(fromDeepObj)
console.log("toDeepObj", JSON.stringify(toDeepObj, null, 2));

let fromDeepObjArr = {
  "arr1": {
    "__arrayLike": true,
    "_alkey_1": {
      "key": "_alkey_1",
      "data": { "name": "Gabin" },
      "index": 0
    },
    "_alkey_2": {
      "key": "_alkey_2",
      "data": { "name": "Charlotte" },
      "index": 1
    },
    "_alkey_3": {
      "key": "_alkey_3",
      "data": { "name": "Dove" },
      "index": 2
    }
  },
  "sub": {
    "arr2": {
      "__arrayLike": true,
      "_alkey_1": {
        "key": "_alkey_1",
        "data": { "name": "Gabin" },
        "index": 0
      },
      "_alkey_2": {
        "key": "_alkey_2",
        "data": { "name": "Charlotte" },
        "index": 1
      },
      "_alkey_3": {
        "key": "_alkey_3",
        "data": { "name": "Dove" },
        "index": 2
      }
    }
  }
}


let toDeepObjArr = arraylike.toArrayDeep(fromDeepObjArr)
console.log("toDeepObjArr", JSON.stringify(toDeepObjArr, null, 2));


let normalObj = {
  users: [
    {
      "name": "Gabin",
      age: 28
    },
    {
      "name": "Charlotte",
      age: 29
    }
  ]
}


let convertedNormalObj = arraylike.convert(normalObj)
console.log("convertedNormalObj", JSON.stringify(convertedNormalObj, null, 2));


let convertedNormalObjToDeepObjArr = arraylike.toArrayDeep(convertedNormalObj)
console.log("convertedNormalObjToDeepObjArr", JSON.stringify(convertedNormalObjToDeepObjArr, null, 2));


let convertedNormalObjToDeepObjArrToObjectDeep = arraylike.toObjectDeep(convertedNormalObjToDeepObjArr)
console.log("convertedNormalObjToDeepObjArrToObjectDeep", JSON.stringify(convertedNormalObjToDeepObjArrToObjectDeep, null, 2));
