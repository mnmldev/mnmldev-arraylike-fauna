const { is, equals } = require("ramda");

const ToObject = arr =>
  arr.reduce((obj, [key, val]) => {
    obj[key] = val;
    return obj;
  }, {});

function compare(oldObj, newObj, remove = false) {
  function recurse(_oldItem, _newItem) {
    if (is(Object, _newItem)) {
      let oldEntries = Object.entries(_oldItem);
      let newEntries = Object.entries(_newItem); // make a Keys fn
      let lostEntries = oldEntries.filter(
        k => !newEntries.map(e => e[0]).includes(k[0])
      ); // use Filter, Not and Contains
      let lostObj = lostEntries.reduce((l, e) => {
        l[e[0]] = null;
        return l;
      }, {}); // Reduce
      let newEntriesFiltered = newEntries.reduce((filtered, [key, item]) => {
        if (!equals(_oldItem[key], item)) {
          filtered.push([
            key,
            is(Object, item) ? recurse(_oldItem[key], item, remove) : item
          ]);
        }
        return filtered;
      }, []);
      if (remove) {
        return Object.assign(lostObj, ToObject(newEntriesFiltered)); // Merge et ToObject
      } else {
        return ToObject(newEntriesFiltered); // ToObject
      }
    } else {
      return _newItem;
    }
  }
  return recurse(oldObj, newObj);
}

console.log(
  compare(
    { un: 1, deux: 2, nested: { un: 1, deux: 2 } },
    { deux: 2, trois: 3, nested: { deux: 2, trois: 3 } },
    false
  )
);
