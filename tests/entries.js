// function entries(input = []) {
//   function recurser(arr = [], acc = []) {
//     if (arr.length === 0) return acc;
//     return recurser(arr.slice(1), [[arr.length - 1, arr[0]], ...acc]); // drop, prepend
//   }
//   function reverse(arr = []) {
//     return arr.reduce((a, b) => [b].concat(a), []);
//   }
//   return recurser(reverse(input));
// }

// console.log(entries(["one", "two"]));

// array.reduce((sorted, el) => {
//   let index = 0;
//   while (index < array.length && el < array[index]) index++;
//   sorted.splice(index, 0, el);
//   return sorted;
// }, []);

function sort(arr, asc = true) {
  function recurser(arr, sorted) {
    if (arr.length === 0) return sorted;
    let itemToAdd = arr.reduce(
      (minItem, item) => (item.index < minItem.index ? item : minItem),
      { index: Infinity }
    );
    return recurser(
      arr.filter(it => it.index !== itemToAdd.index),
      [...sorted, itemToAdd]
    );
  }

  return recurser(arr, []);
}

console.log(
  sort([
    { index: 2, value: "ok" },
    { index: 1, value: "waa" },
    { index: 0, value: "super" }
  ])
);
