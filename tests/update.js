const arraylike = require('./../index')

let updatedObj = {
  users: [
    {
      key: "_alkey_kli9jy4k3a5f8sz",
      data: {
        name: "Gabin",
        age: 28
      }
    },
    {
      data: {
        name: "Charlotte",
        age: 29
      }
    }
  ]
}

let arrayLikeUpdatedObj = arraylike.toArrayLike(updatedObj)
console.log("arrayLikeUpdatedObj", JSON.stringify(arrayLikeUpdatedObj, null, 2));