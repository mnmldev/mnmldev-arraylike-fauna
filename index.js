const {
  Lambda,
  Merge,
  Var,
  NewId,
  Concat,
  Select,
  Let,
  Call,
  StartsWith,
  Equals,
  Not,
  IsInteger,
  ToString,
  ToArray,
  ContainsStr,
  Reduce,
  If,
  Or,
  And,
  Append,
  Drop,
  Prepend,
  Subtract,
  Query
} = require("faunadb");

const ArrayLike_Utils_arraySortAsc_recurser = Query(
  Lambda(
    ["arr", "sorted"],
    Let(
      {
        length: Call("ArrayLike_Utils_arrayLength", Var("arr"))
      },
      If(
        Equals(Var("length"), 0),
        Var("sorted"),
        Let(
          {
            item: Reduce(
              Lambda(
                ["minItem", "item"],
                If(
                  LT(
                    Select("index", Var("item")),
                    Select("index", Var("minItem"))
                  ),
                  Var("item"),
                  Var("minItem")
                )
              ),
              { index: Infinity },
              Var("arr")
            )
          },
          Call("ArrayLike_Utils_arraySortAsc_recurser", [
            Filter(
              Var("arr"),
              Lambda(
                ["it"],
                Not(
                  Equals(
                    Select("index", Var("it")),
                    Select("index", Var("item"))
                  )
                )
              )
            ),
            Append(Var("item"), Var("sorted"))
          ])
        )
      )
    )
  )
);

const ArrayLike_Utils_arraySortAsc = Query(
  Lambda(["arr"], Call("ArrayLike_Utils_entries_recurser", [Var("arr"), []]))
);

const ArrayLike_Utils_arrayLength = Query(
  Lambda(
    ["arr"],
    Reduce(Lambda(["length", "item"], Add(Var("length"), 1)), 0, Var("arr"))
  )
);

const ArrayLike_Utils_arrayReverse = Query(
  Lambda(
    ["arr"],
    Reduce(
      Lambda(["reversed", "item"], Prepend(Var("item"), Var("reversed"))),
      [],
      Var("arr")
    )
  )
);

const ArrayLike_Utils_entries_recurser = Query(
  Lambda(
    ["arr", "acc"],
    Let(
      {
        length: Call("ArrayLike_Utils_arrayLength", Var("arr"))
      },
      If(
        Equals(Var("length"), 0),
        Var("acc"),
        Call("ArrayLike_Utils_entries_recurser", [
          Drop(1, Var("arr")),
          Prepend(
            [Subtract(Var("length"), 1), Select(0, Var("arr"))],
            Var("acc")
          )
        ])
      )
    )
  )
);

const ArrayLike_Utils_entries = Query(
  Lambda(
    ["input"],
    If(
      IsArray(Var("input")),
      Call("ArrayLike_Utils_entries_recurser", [
        Call("ArrayLike_Utils_arrayReverse", [Var("input")]),
        []
      ]),
      If(IsObject(Var("input")), ToArray(Var("input")), [])
    )
  )
);

const ArrayLike_Utils_hasObjectPropery = Lambda(
  ["path", "obj"],
  Not(Equals(Select(path, Var("obj"), "missing"), "missing"))
);

const ArrayLike_defaultOptions = Lambda(
  ["options"],
  Merge(
    {
      prefix: "_al",
      uid_key: "key",
      index_key: "index",
      data_key: "data",
      object_marker: "__arrayLike",
      separator: "_"
    },
    Var("options")
  )
);

const Arraylike_keyCreate = Lambda(
  ["id", "options"],
  Let(
    {
      _id: If(isNull(Var("id")), NewId(), Var("id")),
      _options: Call("ArrayLike_defaultOptions", Var("options"))
    },
    Concat([
      Select("prefix", Var("_options")),
      Select("uid_key", Var("_options")),
      Select("separator", Var("_options")),
      Var("_id")
    ])
  )
);

const ArrayLike_isArrayLikeKey = Lambda(
  ["key", "options"],
  Let(
    {
      _baseKey: Call("Arraylike_keyCreate", ["", Var("options")]),
      _options: Call("ArrayLike_defaultOptions", Var("options"))
    },
    StartsWith(Var("key"), Var("_baseKey"))
  )
);

const ArrayLike_isArrayLikeObject = Lambda(
  ["obj", "options"],
  Let(
    {
      _options: Call("ArrayLike_defaultOptions", Var("options"))
    },
    // Call("ArrayLike_Utils_hasObjectPropery", [
    Contains(
      Select(Select("data_key", Var("_options")), Var("obj")),
      Var("obj")
    )
    // ])
  )
);

const ArrayLike_isArrayLike = Lambda(
  ["obj", "options"],
  Let(
    {
      _options: Call("ArrayLike_defaultOptions", Var("options"))
    },
    // Call("ArrayLike_Utils_hasObjectPropery", [
    Contains(
      Select(Select("object_marker", Var("_options")), Var("obj")),
      Var("obj")
    )
    // ])
  )
);

const ArrayLike_containsArrayLike = Lambda(
  ["input", "options"],
  Let(
    {
      _stringifiedInput: ToString(Var("input")),
      _options: Call("ArrayLike_defaultOptions", Var("options"))
    },
    ContainsStr(
      Var("_stringifiedInput"),
      Select("object_marker", Var("_options"))
    )
  )
);

const ArrayLike_containsArrayLikeArray = Lambda(
  ["input", "options"],
  Let(
    {
      _baseKey: Call("Arraylike_keyCreate", ["", Var("options")]),
      _stringifiedInput: ToString(Var("input")),
      _options: Call("ArrayLike_defaultOptions", Var("options"))
    },
    ContainsStr(Var("_stringifiedInput"), Var("_baseKey"))
  )
);

const ArrayLike_toObject = Lambda(
  ["input", "options"],
  Let(
    {
      _options: Call("ArrayLike_defaultOptions", Var("options"))
    },
    Select(
      "root",
      Reduce(
        Lambda(
          ["obj", "indexValue"],
          Let(
            {
              _indexKey: Select(0, Var("indexValue")),
              _index_isInteger: IsInteger(Select(0, Var("indexValue"))),
              _item: Select(1, Var("indexValue")),
              _itemNeedRecursion: Or(
                IsArray(Select(1, Var("indexValue"))),
                IsObject(Select(1, Var("indexValue")))
              )
            },
            If(
              Var("_index_isInteger"),
              // If an Array
              Let(
                {
                  _itemDataKey: Select(
                    Select("data_key", Var("_options")),
                    Var("_item")
                  ),
                  _itemUidKey: Select(
                    Select("uid_key", Var("_options")),
                    Var("_item")
                  ),
                  _itemDataNeedRecursion: Or(
                    IsArray(
                      Select(Select("data_key", Var("_options")), Var("_item"))
                    ),
                    IsObject(
                      Select(Select("data_key", Var("_options")), Var("_item"))
                    )
                  )
                },
                If(
                  Contains(
                    // Call("ArrayLike_Utils_hasObjectPropery", [
                    Var("_itemDataKey"),
                    Var("_item")
                  ),
                  // ]),
                  Let(
                    {
                      _key: If(
                        Not(Var("_itemUidKey")),
                        Call("Arraylike_keyCreate", [
                          undefined,
                          Var("options")
                        ]),
                        Var("_itemUidKey")
                      )
                    },
                    Merge(Var("obj"), {
                      [Select("object_marker", Var("_options"))]: true,
                      [Var("_key")]: {
                        [Select("uid_key", Var("_options"))]: Var("_key"),
                        [Select("index_key", Var("_options"))]: Var(
                          "_indexKey"
                        ),
                        [Select("data_key", Var("_options"))]: If(
                          Var("_itemDataNeedRecursion"),
                          Call(
                            "ArrayLike_toObject",
                            Select("data", Var("_item"))
                          ),
                          Select("data", Var("_item"))
                        )
                      }
                    })
                  ),
                  Var("obj")
                )
              ),
              // If an Object
              If(
                Var("_itemNeedRecursion"),
                Call(
                  "ArrayLike_toObject",
                  Merge(Var("obj"), {
                    [Var("_indexKey")]: Var("_item")
                  })
                ),
                Merge(Var("obj"), {
                  [Var("_indexKey")]: Var("_item")
                })
              )
            )
          )
        ),
        {},
        {
          root: Call("ArrayLike_Utils_entries", Var("input"))
        }
      )
    )
  )
);

const ArrayLike_toArray = Lambda(
  ["input", "options"],
  Let(
    {
      _options: Call("ArrayLike_defaultOptions", Var("options"))
    },
    Call(
      "ArrayLike_Utils_arraySortAsc",
      Select(
        "root",
        Reduce(
          Lambda(
            ["obj", "indexValue"],
            Let(
              {
                _item: Select(1, Var("indexValue")),
                _item_isObject: IsObject(Select(1, Var("indexValue"))),
                _item_isArrayLike: Call("ArrayLike_isArrayLike", [
                  Select(1, Var("indexValue")),
                  Var("_options")
                ]),
                _itemNeedRecursion: Or(
                  IsArray(Select(1, Var("indexValue"))),
                  IsObject(Select(1, Var("indexValue")))
                ),
                _indexKey: Select(0, Var("indexValue")),
                _index_isInteger: IsInteger(Select(0, Var("indexValue")))
              },
              If(
                And(Var("_item_isObject"), Var("_item_isArrayLike")),
                Let(
                  {
                    _itemArray: Filter(
                      Call("ArrayLike_Utils_entries", [Var("_item")]),
                      Lambda(
                        ["k", "v"],
                        Not(
                          Equals(
                            Select("object_marker", Var("_options")),
                            Var("k")
                          )
                        )
                      )
                    )
                  },
                  Reduce(
                    Lambda(
                      ["arr", "kV"],
                      Let(
                        {
                          _k: Select(0, "kV"),
                          _v: Select(1, "kV"),
                          _keyIsArrayLike: Call("ArrayLike_isArrayLikeKey", [
                            Select(0, "kV"),
                            Var("_options")
                          ])
                        },
                        If(
                          Var("_keyIsArrayLike"),
                          Append(
                            Var("arr"),
                            Merge(Var("_v"), {
                              [Select("uid_key", Var("_options"))]: Var("_k")
                            })
                          )
                        )
                      )
                    ),
                    [],
                    Var("_itemArray")
                  )
                ),
                If(
                  Var("_itemNeedRecursion"),
                  Call(
                    "ArrayLike_toArray",
                    Merge(Var("obj"), {
                      [Var("_indexKey")]: Var("_item")
                    })
                  ),
                  Merge(Var("obj"), {
                    [Var("_indexKey")]: Var("_item")
                  })
                )
              )
            )
          ),
          {},
          { root: Call("ArrayLike_Utils_entries", [Var("input")]) }
        )
      )
    )
  )
);

const ArrayLike_compare_recurser = Query(
  Lambda(
    ["_oldItem", "_newItem", "_remove"],
    If(
      IsObject(Var("_newItem")),
      Let(
        {
          oldEntries: ToArray(Var("_oldItem")),
          newEntries: ToArray(Var("_newItem"))
        },
        Let(
          {
            lostEntries: Filter(
              Var("oldEntries"),
              Lambda(
                "k",
                Not(
                  Reduce(
                    Lambda(
                      ["contains", "item"],
                      Or(
                        Var("contains"),
                        Equals(Select(0, Var("k")), Select(0, Var("item")))
                      )
                    ),
                    false,
                    Var("newEntries")
                  )
                )
              )
            ),
            newEntriesFiltered: Reduce(
              Lambda(
                ["filtered", "keyItem"],
                Let(
                  {
                    key: Select(0, Var("keyItem")),
                    item: Select(1, Var("keyItem"))
                  },
                  If(
                    Not(
                      Equals(
                        Select(Var("key"), Var("_oldItem"), null),
                        Var("item")
                      )
                    ),
                    Append(
                      [
                        [
                          Var("key"),
                          If(
                            IsObject(Var("item")),
                            Call("ArrayLike_compare_recurser", [
                              Select(Var("key"), Var("_oldItem"), null),
                              Var("item"),
                              Var("_remove")
                            ]),
                            Var("item")
                          )
                        ]
                      ],
                      Var("filtered")
                    ),
                    Var("filtered")
                  )
                )
              ),
              [],
              Var("newEntries")
            )
          },
          Let(
            {
              lostObj: ToObject(
                Map(
                  Var("lostEntries"),
                  Lambda("e", [Select(0, Var("e")), null])
                )
              ),
              newEntriesObj: ToObject(Var("newEntriesFiltered"))
            },
            If(
              Var("_remove"),
              Merge(Var("lostObj"), Var("newEntriesObj")),
              Var("newEntriesObj")
            )
          )
        )
      ),
      Var("_newItem")
    )
  )
);

const ArrayLike_compare = Query(
  Lambda(
    ["oldObj", "newObj", "remove"], // Remove : true or false
    Call("ArrayLike_compare_recurser", [
      Var("oldObj"),
      Var("newObj"),
      Var("remove")
    ])
  )
);

// OLD

// convert

function convertToArrayLike(arr = [], options = {}) {
  let opt = defaultOpt(options);
  return Object.entries(arr).reduce(
    (obj, [index, value]) => {
      let key = createKey(opt);
      Object.assign(obj, {
        [key]: {
          [opt.uid_key]: key,
          [opt.index_key]: +index,
          [opt.data_key]: value
        }
      });
      return obj;
    },
    {
      [opt.object_marker]: true
    }
  );
}

function convert(anyObjOrArr = {}, options = {}) {
  let opt = defaultOpt(options);
  return dig(
    (value, index) => {
      if (value && is(Array, value)) {
        return convertToArrayLike(value, opt);
      }
      return value;
    },
    { root: anyObjOrArr }
  ).root;
}

function revert(obj = {}, options = {}) {
  let opt = defaultOpt(options);
  return dig(
    (value, index) => {
      if (value && is(Object, value) && isArrayLike(value)) {
        return toArray(value, opt).map(item => item.data);
      }
      return value;
    },
    { root: obj }
  ).root;
}

module.exports = {
  convert,
  revert,
  toArrayLike,
  fromArrayLike,
  randomKey,
  keyCreate,
  toObject,
  toObjectDeep,
  toArray,
  toArrayDeep,
  isArrayLike,
  isArrayLikeKey,
  isArrayLikeObject,
  containsArrayLike,
  containsArrayLikeArray
};
